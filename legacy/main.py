# USAGE
# python main.py --image pokemon_games.png
# python main.py --image bola.jpg


# import the necessary packages
import numpy as np
import argparse
import cv2
#from screeninfo import get_monitors #Pode ser útil em telas com dois monitores
import pyautogui as mouse #https://github.com/asweigart/pyautogui

#Obtém a resolução
wScreen, hScreen = mouse.size()
#temp  = get_monitors()
#wScreen = temp[0].width
#hScreen = temp[0].height

# construct the argument parse and parse the arguments
ap = argparse.ArgumentParser()
ap.add_argument("-i", "--image", help = "path to the image")
args = vars(ap.parse_args())

#Verifica de se veio alguma arquivo pelo argumento
filename = str(args["image"])
if(filename == 'None'):
	filename = 'bola.jpg'

#carrega a imagem pelo nome do arquivo
image = cv2.imread(filename)
image2 = cv2.imread('Bola2.PNG')
hImage, wImage = image2.shape[:2]

#Monta o retangulo para ser o alvo do corte.
y=131
x=939
h=90
w=h
crop_img = image2[y:y+h, x:x+w]

#Aumenta a imagem para o dobro do tamanho
zoom = 10
height, width = crop_img.shape[:2]
im_resize = cv2.resize(crop_img,(zoom*width, zoom*height), interpolation = cv2.INTER_CUBIC)
#cv2.imshow('image',im_resize)

#Separa as cores do recorte para detectar máximos e mínimos
R = []
G = []
B = []
for x in range(w):
	for y in range(h):
		B.append(crop_img[x,y,0])
		G.append(crop_img[x,y,1])
		R.append(crop_img[x,y,2])

R = [min(R),max(R)]
G = [min(G),max(G)]
B = [min(B),max(B)]
print('R',R)
print('G',G)
print('B',B)

# define the list of boundaries
boundaries = [
	#([17, 15, 100], [50, 56, 200]),
	([B[0],G[0],R[0]], [B[1],G[1],R[1]])
]

# loop over the boundaries

for (lower, upper) in boundaries:
	# create NumPy arrays from the boundaries
	lower = np.array(lower, dtype = "uint8")
	upper = np.array(upper, dtype = "uint8")

	# find the colors within the specified boundaries and apply
	# the mask
	mask = cv2.inRange(image, lower, upper)
	output = cv2.bitwise_and(image, image, mask = mask)

#cv2.imshow("Contornos",output)
# show the images
#cv2.imshow("images", np.hstack([image, output]))
#cv2.imwrite('filtros_'+filename,output)


#Contornos
'''im = output
imgray = cv2.cvtColor(im,cv2.COLOR_BGR2GRAY)
ret,thresh = cv2.threshold(imgray,127,255,0)
contours, hierarchy = cv2.findContours(thresh,cv2.RETR_TREE,cv2.CHAIN_APPROX_SIMPLE)

img = cv2.drawContours(im, contours, -1, (0,255,0), 1)
cv2.imshow('Contornos',img)'''



#Circulos
#img = cv2.imread('opencv-logo2.png',0)
img= cv2.cvtColor(output,cv2.COLOR_BGR2GRAY)
img = cv2.medianBlur(img,5)
cimg = cv2.cvtColor(img,cv2.COLOR_GRAY2BGR)
'''
https://www.pyimagesearch.com/2014/07/21/detecting-circles-images-using-opencv-hough-circles/
cv2.HoughCircles(image, method, dp, minDist)

	image:		8-bit, single channel image. If working with a color image, 
				convert to grayscale first.
	method:		Defines the method to detect circles in images. Currently, 
				the only implemented method is cv2.HOUGH_GRADIENT, which 
				corresponds to the Yuen et al. paper.
	dp:			This parameter is the inverse ratio of the accumulator resolution 
				to the image resolution (see Yuen et al. for more details). 
				Essentially, the larger the dp gets, the smaller the accumulator array gets.
	minDist:	Minimum distance between the center (x, y) coordinates of 
				detected circles. If the minDist is too small, multiple circles 
				in the same neighborhood as the original may be (falsely) detected. 
				If the minDist is too large, then some circles may not be detected at all.
	param1:		Gradient value used to handle edge detection in the Yuen et al. method.
	param2:		Accumulator threshold value for the cv2.HOUGH_GRADIENT method. 
				The smaller the threshold is, the more circles will be detected 
				(including false circles). The larger the threshold is, the more 
				circles will potentially be returned.
	minRadius:	Minimum size of the radius (in pixels).
	maxRadius:	Maximum size of the radius (in pixels).
'''
dp = np.arange(1, 10, 0.1)
for n in range(len(dp)):
	circles = cv2.HoughCircles(img,cv2.HOUGH_GRADIENT,
		dp[n],150,param1=30,param2=30,minRadius=0,maxRadius=0)
	srtCircle = str(circles)
	if(srtCircle != 'None'):
		break
		
print(dp[n] , len(circles[0]))
srtCircle = str(circles)
if(srtCircle != 'None'):
	circles = np.uint16(np.around(circles))
	for i in circles[0,:]:
		# draw the outer circle
		#cv2.circle(cimg,(x,y),Raio,(R,G,B),simbolo)
		cv2.circle(cimg,(i[0],i[1]),i[2],(0,255,0),2)
		# draw the center of the circle
		cv2.circle(cimg,(i[0],i[1]),2,(0,0,255),3)
		print('pixel',i[0],'x',i[1])

	wPos = i[0]
	hPos = i[1]

	#Mostra o recorte da imagem
	h=i[2]
	w=i[2]
	x=i[0]-int(w/2)
	y=i[1]-int(h/2)
	crop_img = image[y:y+h, x:x+w]
	#cv2.imshow('image',crop_img)


	#Normaliza para o tamanho da tela
	wPos = int((wScreen*wPos)/wImage)
	hPos = int((hScreen*hPos)/hImage)
	print('tela ',wScreen,'x',hScreen)
	print('imagem ',wImage,'x',hImage)
	print('posicao ',wPos,'x',hPos)


	mouse.moveTo(wPos,hPos)		# Move para posição
	#mouse.moveRel(0, 10)	# Move mouse 10 pixels para baixo
	#mouse.dragTo(xo, yo)	# Arrasta para posição
	#mouse.click()			# Clica
	#mouse.doubleClick()	# Duplo clique
	#mouse.dragRel(0, 10)	# Arrasta 10 pixels para baixo


cv2.imshow('detected circles',cimg)
#Segura a exibição
cv2.waitKey(0)