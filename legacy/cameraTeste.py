import cv2
import numpy as np
import pyautogui as mouse #https://github.com/asweigart/pyautogui

#Obtém a resolução
wScreen, hScreen = mouse.size()
print(wScreen,hScreen)

#cap = cv2.VideoCapture('bolaVerde.avi')
cap = cv2.VideoCapture(0)
dp = np.arange(1, 10, 0.1)
lastwPos=[1,1,1]
wPos=1
lasthPos=[1,1,1]
hPos = 1
position =0

mediaW = 10
mediaH = 10

# Define the codec and create VideoWriter object
fourcc = cv2.VideoWriter_fourcc(*'XVID')
out = cv2.VideoWriter('TrackingColor.avi',fourcc, 20.0, (640,480))

kernelClose = np.ones((5,5),np.uint8)
kernel = np.ones((5,5),np.float32)/100
_, frame = cap.read()
hImage = frame.shape[0]
wImage = frame.shape[1]
while(1):

	# Take each frame
	_, frame = cap.read()

	frame = cv2.flip(frame, 1)	
	
	# Convert BGR to HSV
	hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)

	# define range of blue color in HSV
	# Azul
	#lower_blue = np.array([60,30,105])
	#upper_blue = np.array([100,90,200])
	#
	#lower_blue = np.array([171,27,42])
	#upper_blue = np.array([178,48,58])
	#lower_blue = np.array([132,36,87])
	#upper_blue = np.array([147,56,100])
	lower_blue = np.array([50,50,50])
	upper_blue = np.array([130,140,200])

	# Threshold the HSV image to get only blue colors
	mask = cv2.inRange(hsv, lower_blue, upper_blue)

	# Bitwise-AND mask and original image
	#res = cv2.bitwise_and(frame,frame, mask= mask)

	res = frame 

	#circles = cv2.HoughCircles(mask,cv2.HOUGH_GRADIENT,4,100)
	#circles = cv2.HoughCircles(mask,cv2.HOUGH_GRADIENT,1.8,120,
	#	param1=50,param2=30,minRadius=5,maxRadius=100)

	for n in range(len(dp)):
		circles = cv2.HoughCircles(mask,cv2.HOUGH_GRADIENT,
			dp[n],120,param1=50,param2=30,minRadius=5,maxRadius=100)
		if circles is not None: break
		
	if circles is not None:                        
		circles = np.uint16(np.around(circles))
		for i in circles[0,:]:
	#        # draw the outer circle
			cv2.circle(frame,(i[0],i[1]),i[2],(0,255,0),2)
	#        # draw the center of the circle
			cv2.circle(frame,(i[0],i[1]),2,(0,0,255),3)
		res = frame
		wPos = i[0]
		hPos = i[1]

		#Normaliza para o tamanho da tela
		wPos = int((wScreen*wPos)/wImage)
		hPos = int((hScreen*hPos)/hImage)
		if(wPos!= 0 and hPos != 0 ):
			#print('tela ',wScreen,'x',hScreen)
			#print('imagem ',wImage,'x',hImage)
			#print('posicao ',wPos,'x',hPos)
			mediaW = int(wPos*0.25 + mediaW * 0.75)
			mediaH = int(hPos*0.25 + mediaH * 0.75)
			mouse.moveTo(mediaW,mediaH)

	out.write(res)
	
	#while(lastwPos != wPos or lasthPos !=hPos):
	'''if(wPos != 0 and hPos != 0):
		 wraio = wPos - lastwPos
		 
		if(lastwPos < wPos - 100): lastwPos += 100
		if(lastwPos < wPos - 10): lastwPos += 10
		if(lastwPos < wPos): lastwPos += 10
		if(lastwPos < wPos - 100): lastwPos += 10
		elif(lastwPos > wPos): lastwPos -=10
		
		if(lasthPos < hPos): lasthPos += 10
		elif(lasthPos > hPos): lasthPos -= 10
		
		mouse.moveTo(lastwPos,lasthPos)		# Move para posição
	'''
	
	
	cv2.imshow('frame',frame)
	cv2.imshow('mask',mask)
	cv2.imshow('res',res)
	k = cv2.waitKey(40) & 0xFF
	if k == 27:
		break

cap.release()
out.release()
cv2.destroyAllWindows()