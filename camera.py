import cv2
import numpy as np
import pyautogui as mouse #https://github.com/asweigart/pyautogui


def createMask (frame, boundUpper,boundLower,type = 'bgr'):
	# find the colors within the specified boundaries and apply
	# the mask
	if(type == 'bgr'):
		mask = cv2.inRange(frame, boundLower, boundUpper)
		output = cv2.bitwise_and(frame, frame, mask = mask)
		img= cv2.cvtColor(output,cv2.COLOR_BGR2GRAY)
		mask = cv2.medianBlur(img,5)
	elif(type == 'hsv'):
		# Convert BGR to HSV
		hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)
		# Threshold the HSV image to get only blue colors
		mask = cv2.inRange(hsv, boundLower, boundUpper)
	return mask

def circulos (mask):
	Pos=[[[0,0,0]]]
	dp = np.arange(1, 3, 0.1)
	valid = False

	for n in range(len(dp)):
		circles = cv2.HoughCircles(mask,cv2.HOUGH_GRADIENT,
			dp[n],120,param1=50,param2=30,minRadius=5,maxRadius=100)
		if circles is not None: break

	if circles is not None:
		Pos = np.uint16(np.around(circles))
		valid = True
		print(round(dp[n],1),circles[0])
	return Pos[0][0], valid


def printCirc(frame,local,color):
	cv2.circle(frame,(local[0],local[1]),local[2],color,2)
	cv2.circle(frame,(local[0],local[1]),2,color,3)
	return frame

def bounderiesSet(frame,recorte,type = 'bgr'):
	x = recorte[0]
	y = recorte[1]
	w = recorte[2]
	h = w
	if(type == 'hsv'):
		frame = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)

	crop_img = frame[y:y+h, x:x+w]
	#Separa as cores do recorte para detectar máximos e mínimos
	var1 = []
	var2 = []
	var3 = []
	for x in range(w):
		for y in range(h):
			var1.append(crop_img[x,y,0])
			var2.append(crop_img[x,y,1])
			var3.append(crop_img[x,y,2])

	l = np.array([min(var1),min(var2),min(var3)])
	u = np.array([max(var1),max(var2),max(var3)])
	print('lower',l)
	print('upper',u)
	return l,u

def positionFilter(pos,cmd='move'):
	global mediaW
	global mediaH
	tempW = 0
	tempH = 0
	pos[2] = pos[2]*1.10	#Acrescenta um percentual para atingir os cantinhos
	if(pos[0]>centro[0]):
		if(pos[0]+pos[2]>wImage): tempW = wImage
		else: tempW = int(pos[0]+pos[2]*(pos[0]-centro[0])/centro[0])
	else:
		if(pos[0]-pos[2]<0): tempW = 0
		else: tempW = int(pos[0]-pos[2]*(centro[0]-pos[0])/centro[0])

	if(pos[1]>centro[1]):
		if(pos[1]+pos[2]>hImage): tempH = hImage
		else: tempH = int(pos[1]+pos[2]*(pos[1]-centro[1])/centro[1])
	else:
		if(pos[1]-pos[2]<0): tempH = 0
		else: tempH = int(pos[1]-pos[2]*(centro[1]-pos[1])/centro[1])
	point = [tempW,tempH,1]

	#Normaliza para o tamanho da tela
	wPos = int((wScreen*point[0])/wImage)
	hPos = int((hScreen*point[1])/hImage)
	if(wPos!= 0 and hPos != 0 ):
		mediaW = int(wPos*0.25 + mediaW * 0.75)
		mediaH = int(hPos*0.25 + mediaH * 0.75)
		if(cmd=='move'): mouse.moveTo(mediaW,mediaH)
		elif(cmd=='drag'): mouse.dragTo(mediaW,mediaH)
	return point


#Obtém a resolução da tela
wScreen, hScreen = mouse.size()
print(wScreen,hScreen)

#Captura a camera
cap = cv2.VideoCapture(0)

#Filtro
mediaW = 10
mediaH = 10
lastwPos=[1,1,1]
lasthPos=[1,1,1]

#Teclas
scape = 27
espace = 32
letter_S = 115

#Resolução do frame
_, frame = cap.read()
hImage = frame.shape[0]
wImage = frame.shape[1]
sampleSize = 50
centro = [int(wImage/2),int(hImage/2),sampleSize]
recorte = [int(centro[0]-sampleSize/(2**(1/2))),int(centro[1]-sampleSize/(2**(1/2))),sampleSize]

#Standar Bounderies
lowerMove = np.array([50,50,50])
upperMove = np.array([130,140,200])

lowerClic = np.array([50,50,50])
upperClic = np.array([130,140,200])

greenColor = (0,255,0)
redColor = (0,0,255)
stepCalibrate = 0
imprime = 0
type = 'bgr'
#type = 'hsv'
while(1):

	# Take each frame
	_, frames = cap.read()

	frames = cv2.flip(frames, 1)
	#Impressão dos bounderies
	if(imprime == 1):
		imprime += 1
		lowerMove,upperMove = bounderiesSet(frames,recorte,type=type)
	elif(imprime == 3):
		imprime += 1
		lowerClic,upperClic = bounderiesSet(frames,recorte,type=type)

	#Estado de Movimentos do Mouse
	if(stepCalibrate == 0):
		frames=printCirc(frames,centro,greenColor)

	#Estado de de capruta do Clique
	elif(stepCalibrate == 1):
		frames=printCirc(frames,centro,redColor)

	#Estado de Execução do Mouse
	elif(stepCalibrate >= 2):
		mask = createMask(frames,upperMove,lowerMove,type=type)
		pos, valid = circulos(mask)
		if(valid):
			cv2.imshow('mask',mask)
			frames=printCirc(frames,pos,greenColor)
			pos = positionFilter(pos,cmd='move')
			frames = printCirc(frames,pos,redColor)
		else:
			mask = createMask(frames,upperClic,lowerClic,type=type)
			pos, valid = circulos(mask)
			cv2.imshow('mask',mask)
			frames=printCirc(frames,pos,redColor)
			pos = positionFilter(pos,cmd='drag')
			frames = printCirc(frames,pos,greenColor)

	cv2.imshow('frame',frames)

	k = cv2.waitKey(40) & 0xFF
	if k == scape:
		break
	elif k == espace:
		stepCalibrate += 1
		imprime +=1
	elif k == letter_S:
		stepCalibrate += 1
		imprime +=2

cap.release()
cv2.destroyAllWindows()